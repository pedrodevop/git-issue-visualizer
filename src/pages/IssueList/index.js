import { connect } from 'react-redux';

import IssueList from './IssueList';

export default connect(
  ({ issues: { issues } }) => ({ issues }),
)(IssueList);