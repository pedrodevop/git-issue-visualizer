import React from 'react';

import './IssueList.css';

import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Avatar from 'material-ui/Avatar';

import FilterBar from '../components/FilterBar';

export default ({ issues }) => (
  <div>
    <FilterBar />

    <div>Issue List</div>

    {issues.map(({ id, title, user, state, html_url }) => {
      return (
        <Paper key={id} className='rowContainer' elevation={4}>
          <div>
            <Avatar src={user.avatar_url}/>
          </div>
          <div>
            <Typography variant="headline" component="h3">
              {title}
            </Typography>
            <Typography component="p">
              Author {user.login} - Status {state} <a target='_blank' href={html_url}>Go to page</a>
            </Typography>
          </div>
        </Paper>
      )
    })}
  </div>
);
