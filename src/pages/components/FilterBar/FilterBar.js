import React from 'react';

import AutocompleteField from '../AutocompleteField';

import './FilterBar.css';

import Toolbar from 'material-ui/Toolbar';
import TextField from 'material-ui/TextField';

function renderDatepicker(value, onChange) {
  return (<TextField
      label='Date'
      value={value}
      onChange={event => onChange(event.target.value)}
      type='date'
      InputLabelProps={{ shrink: true }}
  />);
};

export default ({ options, selected, changeFilterValue }) => {
  const users = options.users.map(u => ({ ...u, label: u.login }));
  const labels = options.labels.map(l => ({ ...l, label: l.name }));
  const milestones = options.milestones.map(m => ({ ...m, label: '' }));
  const state = options.states.map(s => ({ label: s, id: s }));
  const sort = options.sort.map(s => ({ label: s, id: s }));
  const directions = options.direction.map(s => ({ label: s, id: s }));

  const onChange = (key) => (value) => changeFilterValue(key, value);

  const fields = [
    { title: 'Author', value: selected.creator, options: users, onChange: onChange('creator') },
    { title: 'Labels', value: selected.label, options: labels, onChange: onChange('label') },
    { title: 'Milestones', value: selected.milestone, options: milestones, onChange: onChange('milestone') },
    { title: 'Assignee', value: selected.assignee, options: users, onChange: onChange('assignee') },
    { title: 'Mentioned', value: selected.mentioned, options: users, onChange: onChange('mentioned') },
    { title: 'State', width: 70, value: selected.state, options: state, onChange: onChange('state') },
    { title: 'Sort', width: 80, value: selected.sort, options: sort, onChange: onChange('sort') },
    { title: 'Direction', width: 80, value: selected.direction, options: directions, onChange: onChange('direction') },
  ];

  return (
    <div>
      <Toolbar>
        <div className='toolbarContainer'>
          {fields.map(f => <AutocompleteField key={f.title} {...f} /> )}
          {renderDatepicker(selected.since, onChange('since'))}
        </div>
      </Toolbar>
    </div>
  );
};
