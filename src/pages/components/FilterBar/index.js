import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import FilterBar from './FilterBar';

import changeFilterValue from '../../../actions/changeFilterValue';

export default connect(
  ({ filters: { options, selected } }) => ({ options, selected }),
  (dispatcher) => bindActionCreators({ changeFilterValue }, dispatcher)
)(FilterBar);
