import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './AutocompleteField.css';

import ReactAutocomplete from 'react-autocomplete';

import Clear from 'material-ui-icons/Clear';

import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';

export default class AutocompleteField extends Component {

  static propTypes = {
    title: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired,
    value: PropTypes.string,
    renderItem: PropTypes.func,
    onChange: PropTypes.func,
  }

  static defaultProps = {
    value: '',
  }

  state = {
    isOpen: false,
    searchText: '',
    selectedText: '',
    onChange: f => f,
  };

  componentWillReceiveProps({ value }) {
    this.setState({ selectedText: value, searchText: '' });
  }

  clear = () => {
    const { onChange } = this.props;
    this.setState({ searchText: '', selectedText: '' });
    onChange('');
  }

  render() {
    const { options, title, value, renderItem, onChange, width = 170 } = this.props;
    const { searchText, selectedText, isOpen } = this.state;

    return (
      <div className='container' style={{ width }}>
        <ReactAutocomplete
          open={isOpen}
          items={options}
          onChange={event => this.setState({ searchText: event.target.value, selectedText: '' })}
          shouldItemRender={(item) => item.label.toLowerCase().includes(searchText.toLowerCase())}
          onSelect={(value, item) => {
            this.setState({ isOpen: false, searchText: '', selectedText: value });
            onChange(value);
          }}
          value={selectedText || searchText}
          getItemValue={item => item.label}
          inputProps={{
            onFocus: () => this.setState({ isOpen: true }),
            onClick: () => this.setState({ isOpen: true }),
            onBlur: () => this.setState({ isOpen: false }),
          }}
          renderMenu={(items) => {
            return <Paper className='menu'>{items}</Paper>;
          }}
          renderItem={renderItem || function(item, highlighted) {
            return (<div key={item.id} style={{ backgroundColor: highlighted ? '#eee' : 'transparent' }}>
                {item.label}
              </div>);
          }}
          renderInput={props => {
            return <TextField
              className='searchInput'
              label={title}
              value={selectedText || searchText}
              InputProps={!selectedText ? null :
                {endAdornment:<Clear className='clearIcon' fontSize={true} onClick={this.clear}/>}
              }
              inputProps={props}
              />
          }}
        />
      </div>
    );
  }
}