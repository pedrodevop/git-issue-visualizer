import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';

import { issues, filters, repository } from './reducers';
import callRefreshFilters from './middlewares/callRefreshFilters';
import callRefreshIssues from './middlewares/callRefreshIssues';
import changeRepositiory from './actions/changeRepository';

const store = createStore(
  combineReducers({ issues, filters, repository }),
  applyMiddleware(
    thunkMiddleware,
    callRefreshFilters,
    callRefreshIssues
  )
);
store.dispatch(changeRepositiory('atom', 'atom'));

const theme = createMuiTheme();

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider theme={theme}>
      <App />
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
