export default function changeRepository(author, repository) {
  return { type: 'CHANGE_REPOSITORY', payload: { author, repository } };
}
