export default function changeFilterValue(key, value) {
  return { type: 'CHANGE_FILTER_VALUE', payload: { key, value } };
}
