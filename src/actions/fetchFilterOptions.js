function updateOptionsValues(option, values) {
  return { type: 'UPDATE_OPTIONS_VALUES', payload: { key: option, values } };
}

export function fetchLabels() {
  return async (dispatch, getState) => {
    const { author, repository } = getState().repository;

    const response = await fetch(`https://api.github.com/repos/${author}/${repository}/labels`);
    const labels = await response.json();

    dispatch(updateOptionsValues('labels', labels));
  };
};

export function fetchMilestones() {
  return async (dispatch, getState) => {
    const { author, repository } = getState().repository;

    const response = await fetch(`https://api.github.com/repos/${author}/${repository}/milestones`);
    const milestones = await response.json();

    dispatch(updateOptionsValues('milestones', milestones));
  };
};

export function fetchAssignees() {
  return async (dispatch, getState) => {
    const { author, repository } = getState().repository;

    const response = await fetch(`https://api.github.com/repos/${author}/${repository}/assignees`);
    const assignees = await response.json();

    dispatch(updateOptionsValues('users', assignees));
  };
};
