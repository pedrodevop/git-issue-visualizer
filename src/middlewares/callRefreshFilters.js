import { fetchLabels, fetchMilestones, fetchAssignees } from '../actions/fetchFilterOptions';

export default store => next => action => {
  const result = next(action);
  if (action.type === 'CHANGE_REPOSITORY') {
    store.dispatch(fetchLabels());
    store.dispatch(fetchMilestones());
    store.dispatch(fetchAssignees());
  }

  return result;
};