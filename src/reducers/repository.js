const defaultState = {
  author: null,
  repository: null,
}

export default (state = defaultState, { payload, type }) => {
  switch (type) {
    case 'CHANGE_REPOSITORY': {
      return {
        ...state,
        author: payload.author,
        repository: payload.repository,
      }
    }
    default:
      return state;
  }
};