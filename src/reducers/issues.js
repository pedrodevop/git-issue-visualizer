const defaultState = {
  issues: [],
};

export default (state = defaultState, { payload, type }) => {
  switch (type) {
    case 'UPDATE_ISSUES':
      return {
        ...state,
        issues: payload.issues
      };
    default:
      return state;
  }
};
