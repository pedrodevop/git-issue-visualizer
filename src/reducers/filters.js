const defaultState = {
  options: {
    milestones: [],
    states: ['open', 'closed', 'all'],
    users: [],
    labels: [],
    sort: ['created', 'updated', 'comments'],
    direction: ['asc', 'desc']
  },
  selected: {
    milestone: null,
    state: 'open',
    assignee: null,
    creator: null,
    mentioned: null,
    label: null,
    sort: 'created',
    direction: 'desc',
    since: '',
  }
};

export default (state = defaultState, { payload, type }) => {
  switch (type) {
    case 'CHANGE_FILTER_VALUE': {
      const { key, value } = payload;

      return {
        ...state,
        selected: {
          ...state.selected,
          [key]: value,
        },
      };
    }
    case 'UPDATE_OPTIONS_VALUES': {
      const { key, values } = payload;

      return {
        ...state,
        options: {
          ...state.options,
          [key]: values,
        }
      };
    }
    default:
      return state;
  }
};
